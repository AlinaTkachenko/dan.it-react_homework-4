import cn from "classnames";

const ModalBody = ({children, optionalClassName}) => {

    return (
        <div className={cn('modal__body', optionalClassName)}>
            {children}
        </div>
    )
}

export default ModalBody;