import { useState } from 'react';
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { setCurrentCard, toggleCartProduct, toggleModal } from "../../store/mainSlice";

import "./Counter.scss";

const Counter = ({product}) => {
    const [counter, setCounter] = useState(product.count || 1);
    const dispatch = useDispatch();

    const increaseCount = () =>  {
        setCounter(+counter+1)
      }

    const decreaseCount = () => {
        if(counter > 1 ){
            setCounter(+counter-1)
        } 
      }  

    return (
        <div className="counter row">
            <span onClick={() => {
                decreaseCount();
                if(counter === 1) {
                    dispatch(toggleModal("modalText"));
                    dispatch(setCurrentCard(product));
                }else {
                    dispatch(toggleCartProduct({currentCard: product, action: "minus"}));
                }
            }
            }>
                <img src="./images/icons/minus.png"></img>
            </span>
            <input type="text" value={counter} readOnly/>
            <span onClick={() => {
                increaseCount();
                dispatch(toggleCartProduct({currentCard: product, action: "plus"}));
                }
                }>
                <img src="./images/icons/plus.png"></img>
            </span>
        </div>
    )
}

Counter.propTypes = {
    product: PropTypes.object,
  };

export default Counter;