import PropTypes from 'prop-types';

const ImageProductCard = ({src, alt, optionalClassNames}) => {
    return (
        <div className={optionalClassNames}>
            <img src={src} alt={alt}></img>
        </div>
    )
}

ImageProductCard.defaultProps = {
    alt: "bike"
  };

ImageProductCard.propTypes = {
    src: PropTypes.string,
    alt: PropTypes.string,
    optionalClassNames: PropTypes.string
  };

export default ImageProductCard;