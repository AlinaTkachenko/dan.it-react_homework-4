import NavigationItem from "./NavigationItem";

const Navigation = () => {
    const menuList = [{name: 'Головна', path: '/'},
    {name: 'Товари', path: '#','subnavigation': true},
    {name: 'Про нас', path: '/about'},
    {name: 'Підтримка', path: '/support'},
    {name: 'Контакти', path: '/contacts'}]

    const menuItems = menuList.map((item, index) =>
    <NavigationItem key={index} to={item.path} name={item.name} subnavigation={item.subnavigation}/>
  );

    return (
        <nav className="navigation">
            <ul className="navigation__list row">
                {menuItems}
            </ul>
        </nav>
    )
}
export default Navigation;