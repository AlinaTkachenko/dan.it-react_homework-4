import {Link} from 'react-router-dom';

import "./Header.scss";

import Logo from "../Logo/Logo";
import Navigation from "./HeaderComponents/Navigation";
import Favorites from "./HeaderComponents/Favorites";
import IconCart from "./HeaderComponents/IconCart";
import IconPerson from './HeaderComponents/IconPerson';

const Header = () => {
    return(
        <div className="header">
            <div className="header__content content row">
                <Logo />
                <div className="header__wrapper row">
                    <Navigation />
                    <div className="header__icons row">
                        <Link to='/person'><IconPerson src="/images/icons/person.png" alt="person" width="24px" height="24px" /></Link>
                        <Link to='/cart'><IconCart /></Link>
                        <Link to='/favorites'><Favorites /></Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Header;