import { Routes, Route } from 'react-router-dom';

import Layout from '../Layout/Layout';
import Main from '../../Pages/Main/Main';
import Catalog from '../../Pages/Catalog/Catalog';
import Product from '../../Pages/Product/Product';
import About from '../../Pages/About/About';
import Support from '../../Pages/Support/Support';
import Contacts from '../../Pages/Contacts/Contacts';
import Person from '../../Pages/Person/Person';
import Favorites from '../../Pages/Favorites/Favorites';
import Cart from '../../Pages/Cart/Cart';

const AppRoutes = () => {

    return(
        <>
            <Routes>
                <Route path="/" element={<Layout />}>
                <Route index element={<Main />}/>
                <Route path="catalog/:category" element={<Catalog />}/>
                <Route path="catalog/:category/:product" element={<Product />}/>
                <Route path="about" element={<About />}/>
                <Route path="support" element={<Support />}/>
                <Route path="contacts" element={<Contacts />}/>
                <Route path="person" element={<Person />}/>
                <Route path="cart" element={<Cart />}/>
                <Route path="favorites" element={<Favorites />}/>
                </Route>
            </Routes>
        </>
    )
}

export default AppRoutes;