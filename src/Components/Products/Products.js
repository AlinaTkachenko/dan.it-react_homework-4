import PropTypes from "prop-types";

import "./Products.scss";

import ProductCard from "../ProductCard/ProductCard";
import ModalCart from '../Modal/ModalCart/ModalCart';
import Empty from "../Empty/Empty";

const Products = ({ category,products }) => {
    
    return (
        <>
            {products.length === 0 && <Empty />}
            <div className="products">
                {products.length > 0 && products.map((product)=><ProductCard 
                key={product.id} 
                product={product}
                category={category}></ProductCard>)}
            </div>
            <ModalCart />
        </>
    )
}

Products.propTypes = {
    category: PropTypes.string,
    products: PropTypes.array
  };

export default Products;