import "./Cart.scss";

import Title from "../../Components/Title/Title";
import CartProducts from "./CartComponents/CartProducts";

const Cart = () => { 

    return (
        <div className="cart">
            <div className="interior-content">
                <Title>Кошик</Title>
                <CartProducts />
            </div>
        </div>
    )
}

export default Cart; 