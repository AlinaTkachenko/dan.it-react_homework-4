import { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts } from "../../store/mainSlice";

import "./Catalog.scss";

import Products from "../../Components/Products/Products";

const Catalog = () => {
    const products = useSelector(state => state.main.products); 
    const dispatch = useDispatch();
    const { category } = useParams();    
    
    useEffect(() => { 
        dispatch(fetchProducts(category))
    }, [category]) 

    return (
        <div className="catalog">
            <Products 
                category={category}
                products={products}
            />
        </div>
    )
}

export default Catalog;