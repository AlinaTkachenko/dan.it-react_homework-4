import React, { useState } from 'react';
import {useSelector} from "react-redux";

import "./Favorites.scss";

import Title from "../../Components/Title/Title";
import Products from "../../Components/Products/Products";

const Favorites = () => {
    const favorites = useSelector(state => state.main.favorites);

    return (
        <div className="favorites">
            <div className="interior-content">
                <Title>Обране</Title>
                <Products products={favorites} />
            </div>
        </div>
    )
}

export default Favorites; 